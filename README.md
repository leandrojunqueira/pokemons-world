# Pokemon World

Heroku url
https://pokemon-showpad.herokuapp.com

## Requiments:

- A paginated and searchable list of Pokemon
- Detail page with moves and stat attributes (as many as you'd like to include from the various
  APIs)
- A way to save pokemon to a personal list ("Pokemon I've caught")
- A way to save pokemon I'd like to catch ("Pokemon Wishlist)
- Anything else you think would be cool!

## My solutions covered:

- Design mockups of the application.
- Api rest / call service fetching data from api pokemon API url.
  - I started by using the Api Pokemon Grahpql but I faced some bugs to getting sprites images from the api by using Graphql.
- Create a list at home Pokemon List.
- Search bar from pokemon list.
- Pokemon page inlcuding details Moves, Stats...

* start application:
  - clone repo to your local machine.
  - run: npm install
  - run: ng serve

## Missing & Features

- Favorites, My pokemons...

  - My idea was create a Drag and Drop functionalite to Catch Up the pokemon by draging the pokeball icon from the nav bottom over any pokemon from the list.
  - A function randomly "Catch or Not pokemon" to add in the list Pokemon I've caught.
  - Drag and drop to add to the Favorite.

  # Mockup design

  Figma project: https://www.figma.com/file/Rm0b0TyuJSdur9XMpSxyTf/PokemonWorld?node-id=0%3A1

  ![Alt-Text](/mockups.png)
