import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChildRouting } from './child.routing.module';
import { ThemeModule } from '../theme/theme.module';
import { HomeComponent } from './home/home.component';
import { PokemonPageComponent } from './pokemon-page/pokemon-page.component';
import { MyPokemonsComponent } from './my-pokemons/my-pokemons.component';

@NgModule({
  declarations: [HomeComponent, PokemonPageComponent, MyPokemonsComponent],
  imports: [CommonModule, ChildRouting, ThemeModule],
})
export class PagesModule {}
