import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PagesModule } from './pages/pages.module';

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, AppRoutingModule, HttpClientModule, PagesModule],
  providers: [
    {
      provide: 'pokemonAPI',
      useValue: environment.apiPokemon,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
