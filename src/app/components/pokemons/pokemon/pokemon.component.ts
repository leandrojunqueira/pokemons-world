import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { tap, map, filter } from 'rxjs/operators';
import { ApiRequestService } from 'src/app/services/api-request.service';

@Component({
  selector: 'component-pokemon',
  templateUrl: './pokemon.component.html',
  styleUrls: ['./pokemon.component.scss'],
})
export class PokemonComponent implements OnInit {
  constructor(
    private apiRequestService: ApiRequestService,
    private activatedRoute: ActivatedRoute
  ) {}

  public pokemonData: any;
  public movesData: any;
  public stats: any;

  get pokemon() {
    const id = this.activatedRoute.snapshot.params['id'];
    return id;
  }

  get allData() {
    const data = this.apiRequestService.getSingleById(this.pokemon).pipe(
      tap((res) => res),
      map((res) => {
        this.pokemonData = res;
      
      })
    );
    return data;
  }

  ngOnInit(): void {
    this.allData.subscribe();
  }
}
