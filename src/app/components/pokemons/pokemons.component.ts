import { Component, OnInit } from '@angular/core';
import { ApiRequestService } from 'src/app/services/api-request.service';

@Component({
  selector: 'app-pokemons',
  templateUrl: './pokemons.component.html',
  styleUrls: ['./pokemons.component.scss'],
})
export class PokemonsComponent implements OnInit {
  public getAll: any;
  private setAll: any;

  public errorFecth: boolean = false;

  constructor(private apiRequestService: ApiRequestService) {}

  public search(value: string) {
    console.log('value', value);
    const filterSearch = this.setAll.filter((res: any) => {
      return !res.name.indexOf(value.toLowerCase());
    });

    this.getAll = filterSearch;
    console.log('set', this.setAll);
    console.log('get', this.getAll);
  }

  ngOnInit(): void {
    this.apiRequestService.getAllPokemons.subscribe(
      (res) => {
        (this.setAll = res), (this.getAll = this.setAll);
      },
      (error) => (this.errorFecth = true)
    );
  }
}
