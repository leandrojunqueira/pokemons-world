export interface Pokemon {
  results: Pokemon[];
  name: string;
  url: string;
  sprites: [];
  front_shiny: string;
}

export interface Favorite {
  id: number;
  name: string;
}
