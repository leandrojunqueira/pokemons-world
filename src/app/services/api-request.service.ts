import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';

import { map, tap, switchMap } from 'rxjs/operators';
import { Observable, forkJoin } from 'rxjs';
import { Pokemon } from './interface/schema';

@Injectable({
  providedIn: 'root',
})
export class ApiRequestService {
  constructor(
    private http: HttpClient,
    @Inject('pokemonAPI') private pokemonAPI: string
  ) {
    //url api pokemon
    // console.log(this.pokemonAPI);
  }

  getSingleById(id: number): Observable<unknown> {
    return this.http
      .get<Pokemon>(this.pokemonAPI + 'pokemon/' + id)
      .pipe(tap((res) => console.log(res)));
  }

  public getPokemonData(array: Pokemon[]): Observable<Pokemon>[] {
    const results: any = [];
    array.forEach((element) => {
      results.push(this.http.get<Pokemon>(element.url));
    });
    return results;
  }

  get getAllPokemons(): Observable<unknown> {
    const all = '?limit=200';
    return this.http.get<Pokemon>(this.pokemonAPI + 'pokemon' + all).pipe(
      tap((res) => console.log(res)),
      map((res) => res.results),
      switchMap((response) => forkJoin(this.getPokemonData(response)))
    );
  }
}
