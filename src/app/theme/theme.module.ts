import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ContentComponent } from './content/content.component';
import { NavBottomComponent } from './nav-bottom/nav-bottom.component';
import { SearchComponent } from './search/search.component';
import { PokemonsComponent } from '../components/pokemons/pokemons.component';
import { PokemonComponent } from '../components/pokemons/pokemon/pokemon.component';

@NgModule({
  declarations: [
    ContentComponent,
    NavBottomComponent,
    SearchComponent,
    PokemonsComponent,
    PokemonComponent,
  ],
  exports: [
    ContentComponent,
    NavBottomComponent,
    SearchComponent,
    PokemonsComponent,
    PokemonComponent,
  ],
  imports: [CommonModule, RouterModule],
})
export class ThemeModule {}
