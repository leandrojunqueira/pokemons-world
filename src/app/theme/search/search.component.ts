import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
})
export class SearchComponent implements OnInit {
  @Output() public searchEvent: EventEmitter<string> = new EventEmitter();

  constructor() {}

  public pokemonSearch(value: string) {
    console.log('test search', value);
    this.searchEvent.emit(value);
  }

  ngOnInit(): void {}
}
